#!/usr/bin/env python
# coding: utf-8

# In[2]:


import tensorflow as tf


# In[3]:


get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')


# In[4]:


import numpy as np


# In[5]:


hello = tf.constant('Hello, TensorFlow!')


# In[6]:


sess= tf.compat.v1.Session()


# In[7]:


print(sess.run(hello))


# In[8]:


samples =100
xs =np.linspace(-5, 5, samples)


# In[9]:


xs


# In[10]:


ys = np.sin(xs) + np.random.uniform(-0.5, 0.5, samples)


# In[11]:


ys


# In[12]:


inputX= tf.compat.v1.placeholder(tf.float32)
OutputY= tf.compat.v1.placeholder(tf.float32)


# In[13]:


W=tf.Variable(tf.random.normal ([1]), name='weight')
B= tf.Variable(tf.random.normal ([1]), name='bias')


# In[14]:


predictions= tf.add(tf.multiply(inputX,W),B)


# In[15]:


t_loss= tf.reduce_sum(tf.pow(predictions-OutputY,2))/(samples-1) 


# In[17]:


learning_rate = 0.001
optimize_op= tf.train.GradientDescentOptimizer(learning_rate).minimize(t_loss)


# In[18]:


session = tf.Session()
session.run(tf.global_variables_initializer())


# In[19]:


epochs = 1000
previous_loss = 0.0
for epoch in range(epochs):
    for (inputs, outputs) in zip(xs, ys):
        #TODO run the optimize op
        session.run(optimize_op, feed_dict={inputX:inputs, OutputY:outputs})

    # TODO compute the current loss by running the loss operation with the
    # required inputs
    loss = session.run(t_loss, feed_dict={inputX:xs, OutputY:ys})
    print('Training cost = {}'.format(loss))

    # Termination condition for the optimization loop
    if np.abs(previous_loss - loss) < 0.000001:
        break

    previous_loss = loss


# In[20]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
predictions = session.run(W)* xs + session.run(B)
plt.plot(xs, predictions)
plt.plot(xs,ys)
plt.show()


# In[ ]:




