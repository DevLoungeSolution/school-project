#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import tensorflow as tf


# In[ ]:


cluster= tf.train.ClusterSpec({"local" : ["localhost:2222", "localhost:2223"]})


# In[ ]:


task_input = tf.placeholder(tf.float32, 100)


# In[ ]:


with tf.device("/job:local/task:0"):
    local_input = tf.slice(task_input, [50], [-1])
    local_mean = tf.reduce_mean(local_input)


# In[ ]:


with tf.device ("job:local/task:1")
    first_batch = tf.slice(x, [0],[50])
    mean1= tf.reduce_mean(first_batch)


# In[ ]:


with tf.device ("job:local/task:0")
    second_batch = tf.slice(x,[50],[-1])
    mean2= tf.reduce_mean(second_batch)
    
    mean=(mean1+mean2)/2


# In[ ]:


with tf.Session("grpc://localhost:2222") as sess:
    # Sample some data for the computation
    result=sess.run(mean,feed_dict={x:np.random.random(100)})
    print(result)

